@extends('adminlte::layouts.app')

@section('contentheader_title')
Lista de Cores
@endsection

@section('main-content')
    <div class="container-fluid ">
        <h2>Cores</h2>

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Cor</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cores as $c)
                    <tr>
                        <td>{{ $c->id }}</td>
                        <td>{{ $c->cor }}</td>

                        <td>
                        <a href="{{ route('cores.edit', ['id'=>$c->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="{{ route('cores.destroy', ['id'=>$c->id]) }}" class="btn-sm btn-danger">Excluir</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('cores.create') }}" class="btn-sm btn-info">Novo</a>
    </div>
@endsection
