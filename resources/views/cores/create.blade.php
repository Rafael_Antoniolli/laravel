@extends('adminlte::layouts.app')

@section('contentheader_title')
Cadastro de Cores
@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Nova Cor</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['route'=>'cores.store']) !!}

        <div class="form-group">
            {!! Form::label('cor', 'Cor:') !!}
            {!! Form::text('cor', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Criar Cor', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

