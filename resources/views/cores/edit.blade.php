@extends('adminlte::layouts.app')

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Editando Cor: {{$cores->cor}}</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        
        {!! Form::open(['route'=>["cores.update", $cores->id], 'method'=>'put']) !!}
        
        <div class="form-group">
            {!! Form::label('cor', 'Cor:') !!}
            {!! Form::text('cor', $cores->cor, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Cor', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

