<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Acessos</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('profile') }}"><i class='fa fa-home'></i> <span>Profile</span></a></li>
            <li><a href="{{ url('camisetas') }}"><i class='fa fa-tags'></i> <span>Camisetas</span></a></li>
            <li><a href="{{ url('marcas') }}"><i class='fa fa-tags'></i> <span>Marcas</span></a></li>
            <li><a href="{{ url('tamanhos') }}"><i class='fa fa-tags'></i> <span>Tamanhos</span></a></li>
            <li><a href="{{ url('cores') }}"><i class='fa fa-tags'></i> <span>Cores</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
