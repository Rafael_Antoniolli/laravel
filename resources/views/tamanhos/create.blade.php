@extends('adminlte::layouts.app')

@section('contentheader_title')
Cadastro de Tamanhos
@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Novo Tamanho</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['route'=>'tamanhos.store']) !!}

        <div class="form-group">
            {!! Form::label('tamanho', 'Tamanho:') !!}
            {!! Form::text('tamanho', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Criar Tamanho', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

