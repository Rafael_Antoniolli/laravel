@extends('adminlte::layouts.app')

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Editando Tamanhos: </h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        
        {!! Form::open(['route'=>["tamanhos.update", $tamanhos->id], 'method'=>'put']) !!}
        
        <div class="form-group">
            {!! Form::label('tamanho', 'Tamanho:') !!}
            {!! Form::text('tamanho', $tamanhos->tamanho, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Tamanho', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

