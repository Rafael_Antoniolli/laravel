@extends('adminlte::layouts.app')

@section('contentheader_title')
Lista de Tamanhos
@endsection

@section('main-content')
    <div class="container-fluid ">
        <h2>Tamanhos</h2>

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Tamanho</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tamanhos as $t)
                    <tr>
                        <td>{{ $t->id }}</td>
                        <td>{{ $t->tamanho }}</td>

                        <td>
                        <a href="{{ route('tamanhos.edit', ['id'=>$t->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="{{ route('tamanhos.destroy', ['id'=>$t->id]) }}" class="btn-sm btn-danger">Excluir</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('tamanhos.create') }}" class="btn-sm btn-info">Novo</a>
    </div>
@endsection
