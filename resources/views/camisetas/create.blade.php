@extends('adminlte::layouts.app')

@section('contentheader_title')
Cadastro de Camisetas
@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Nova Camiseta</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['route'=>'camisetas.store']) !!}

        <div class="form-group">
            {!! Form::label('marca_id', 'Marca: ') !!}
            {{ Form::select('marca_id', \App\Marca::orderBy('nome')->pluck('nome', 'id')->toArray(), null, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::label('tamanho_id', 'Tamanho: ') !!}
            {{ Form::select('tamanho_id', \App\Tamanho::orderBy('tamanho')->pluck('tamanho', 'id')->toArray(), null, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::label('cor_id', 'Cor: ') !!}
            {{ Form::select('cor_id', \App\Cor::orderBy('cor')->pluck('cor', 'id')->toArray(), null, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::submit('Criar Camiseta', ['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

