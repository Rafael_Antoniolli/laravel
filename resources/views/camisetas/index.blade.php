@extends('adminlte::layouts.app')

@section('contentheader_title')
Lista de Camisetas
@endsection

@section('main-content')
    <div class="container-fluid ">
        <h2>Camisetas</h2>

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Marca</th>
                    <th>Tamanho</th>
                    <th>Cor</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($camisetas as $ca)
                    <tr>
                        <td>{{ $ca->marca->nome }}</td>
                        <td>{{ $ca->tamanho->tamanho }}</td>
                        <td>{{ $ca->cor->cor }}</td>
                        <td>
                        <a href="{{ route('camisetas.edit', ['id'=>$ca->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="{{ route('camisetas.destroy', ['id'=>$ca->id]) }}" class="btn-sm btn-danger">Excluir</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('camisetas.create') }}" class="btn-sm btn-info">Novo</a>
    </div>
@endsection
