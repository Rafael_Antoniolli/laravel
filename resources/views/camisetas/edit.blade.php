@extends('adminlte::layouts.app')

@section('contentheader_title')
Cadastro de Camisetas
@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Editando Camiseta: {{$camisetas->marca->nome}}</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        
        {!! Form::open(['route'=>["camisetas.update", $camisetas->id], 'method'=>'put']) !!}
        
        <div class="form-group">
            {!! Form::label('marca_id', 'Marca: ') !!}
            {{ Form::select('marca_id', \App\Marca::orderBy('nome')->pluck('nome', 'id')->toArray(), $camisetas->marca_id, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::label('tamanho_id', 'Tamanho: ') !!}
            {{ Form::select('tamanho_id', \App\Tamanho::orderBy('tamanho')->pluck('tamanho', 'id')->toArray(), $camisetas->tamanho_id, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::label('cor_id', 'Cor: ') !!}
            {{ Form::select('cor_id', \App\Cor::orderBy('cor')->pluck('cor', 'id')->toArray(), $camisetas->cor_id, ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Camiseta', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

