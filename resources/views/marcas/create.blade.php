@extends('adminlte::layouts.app')

@section('contentheader_title')
Cadastro de Marcas
@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Nova Marca</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['route'=>'marcas.store']) !!}

        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('empresa', 'Empresa:') !!}
            {!! Form::text('empresa', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Criar Marca', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

