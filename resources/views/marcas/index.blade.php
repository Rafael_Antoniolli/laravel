@extends('adminlte::layouts.app')

@section('contentheader_title')
Lista de Marcas
@endsection

@section('main-content')
    <div class="container-fluid ">
        <h2>Marcas</h2>

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Empresa</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($marcas as $m)
                    <tr>
                        <td>{{ $m->id }}</td>
                        <td>{{ $m->nome }}</td>
                        <td>{{ $m->empresa }}</td>

                        <td>
                        <a href="{{ route('marcas.edit', ['id'=>$m->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="{{ route('marcas.destroy', ['id'=>$m->id]) }}" class="btn-sm btn-danger">Excluir</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('marcas.create') }}" class="btn-sm btn-info">Novo</a>
    </div>
@endsection
