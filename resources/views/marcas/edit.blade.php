@extends('adminlte::layouts.app')

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid">
        <h1>Editando Marca: {{$marcas->nome}}</h1>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        
        {!! Form::open(['route'=>["marcas.update", $marcas->id], 'method'=>'put']) !!}
        
        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', $marcas->nome, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('empresa', 'Empresa:') !!}
            {!! Form::text('empresa', $marcas->empresa, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Marca', ['class'=>'btn-sm btn-info']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

