<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix'=>'camisetas'], function() {
        Route::get("",             ['as' => 'camisetas',         'uses' => "CamisetasController@index"]);
        Route::get("create",       ['as' => 'camisetas.create',  'uses' => "CamisetasController@create"]);
        Route::get("{id}/edit",    ['as' => 'camisetas.edit',    'uses' => "CamisetasController@edit"]);
        Route::get("{id}/destroy", ['as' => 'camisetas.destroy', 'uses' => "CamisetasController@destroy"]);
        Route::post("store",       ['as' => 'camisetas.store',   'uses' => "CamisetasController@store"]);
        Route::put("{id}/update",  ['as' => 'camisetas.update',  'uses' => "CamisetasController@update"]);
      });

      Route::group(['prefix'=>'marcas'], function() {
        Route::get("",             ['as' => 'marcas',         'uses' => "MarcasController@index"]);
        Route::get("create",       ['as' => 'marcas.create',  'uses' => "MarcasController@create"]);
        Route::get("{id}/edit",    ['as' => 'marcas.edit',    'uses' => "MarcasController@edit"]);
        Route::get("{id}/destroy", ['as' => 'marcas.destroy', 'uses' => "MarcasController@destroy"]);
        Route::post("store",       ['as' => 'marcas.store',   'uses' => "MarcasController@store"]);
        Route::put("{id}/update",  ['as' => 'marcas.update',  'uses' => "MarcasController@update"]);
      });

      Route::group(['prefix'=>'tamanhos'], function() {
        Route::get("",             ['as' => 'tamanhos',         'uses' => "TamanhosController@index"]);
        Route::get("create",       ['as' => 'tamanhos.create',  'uses' => "TamanhosController@create"]);
        Route::get("{id}/edit",    ['as' => 'tamanhos.edit',    'uses' => "TamanhosController@edit"]);
        Route::get("{id}/destroy", ['as' => 'tamanhos.destroy', 'uses' => "TamanhosController@destroy"]);
        Route::post("store",       ['as' => 'tamanhos.store',   'uses' => "TamanhosController@store"]);
        Route::put("{id}/update",  ['as' => 'tamanhos.update',  'uses' => "TamanhosController@update"]);
      });

      Route::group(['prefix'=>'cores'], function() {
        Route::get("",             ['as' => 'cores',         'uses' => "CoresController@index"]);
        Route::get("create",       ['as' => 'cores.create',  'uses' => "CoresController@create"]);
        Route::get("{id}/edit",    ['as' => 'cores.edit',    'uses' => "CoresController@edit"]);
        Route::get("{id}/destroy", ['as' => 'cores.destroy', 'uses' => "CoresController@destroy"]);
        Route::post("store",       ['as' => 'cores.store',   'uses' => "CoresController@store"]);
        Route::put("{id}/update",  ['as' => 'cores.update',  'uses' => "CoresController@update"]);
      });
});

Auth::routes();

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile/update', 'ProfileController@updateProfile')->name('profile.update');