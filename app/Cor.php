<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cor extends Model
{
    protected $fillable = ['cor'];

    public function camisetas() {
        return $this->hasMany('App\Camiseta');
    }
}
