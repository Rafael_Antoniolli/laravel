<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;
use App\Http\Requests\MarcaRequest;

class MarcasController extends Controller
{
    public function index() {
        $marcas = Marca::all();
        return view('marcas.index', compact('marcas'));
    }

    public function create() {
        return view('marcas.create');
    }

    public function store(MarcaRequest $request) {
        $nova_marca = $request->all();
        Marca::create($nova_marca);
        return redirect()->route('marcas');
    }

    public function destroy($id) {
        Marca::find($id)->delete();
        return redirect()->route('marcas');
    }

    public function edit($id) {
        $marcas = Marca::find($id);
        return view('marcas.edit', compact('marcas'));
    }

    public function update(MarcaRequest $request, $id) {
        Marca::find($id)->update($request->all());
        return redirect()->route('marcas');
    }
}