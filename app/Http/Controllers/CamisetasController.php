<?php

namespace App\Http\Controllers;

use App\Camiseta;
use Illuminate\Http\Request;
use App\Http\Requests\CamisetaRequest;

class CamisetasController extends Controller
{
    public function index() {
        $camisetas = Camiseta::all();
        return view('camisetas.index', compact('camisetas'));
    }

    public function create() {
        return view('camisetas.create');
    }

    public function store(CamisetaRequest $request) {
        $nova_camiseta = $request->all();
        Camiseta::create($nova_camiseta);
        return redirect()->route('camisetas');
    }

    public function destroy($id) {
        Camiseta::find($id)->delete();
        return redirect()->route('camisetas');
    }

    public function edit($id) {
        $camisetas = Camiseta::find($id);
        return view('camisetas.edit', compact('camisetas'));
    }

    public function update(CamisetaRequest $request, $id) {
        Camiseta::find($id)->update($request->all());
        return redirect()->route('camisetas');
    }
}
