<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Traits\UploadTrait;

class ProfileController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('auth.profile');
    }

    public function updateProfile(Request $request)
    {
        // Validacao
        $request->validate([
            'name'              =>  'required',
            'profile_image'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        // pegar usuario atual
        $user = User::findOrFail(auth()->user()->id);

        // setar nome
        $user->name = $request->input('name');

        // Check se foi feito upload da imagem
        if ($request->has('profile_image')) {
            // pega imagem
            $image = $request->file('profile_image');

            // Cria o nome da imagem baseado no usuario e no timestamp
            $name = str_slug($request->input('name')).'_'.time();

            // Define caminho
            $folder = '/uploads/images/';

            // Define caminho aonde imagem sera mantida [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();

            // Upload imagem
            $this->uploadOne($image, $folder, 'public', $name);

            $user->profile_image = $filePath;
        }

        // salva no banco
        $user->save();

        // retorna mensagem de sucesso para usuario
        return redirect()->back()->with(['status' => 'Perfil Atualizado com sucesso.']);
    }
}
