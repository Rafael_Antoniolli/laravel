<?php

namespace App\Http\Controllers;

use App\Tamanho;
use Illuminate\Http\Request;
use App\Http\Requests\TamanhoRequest;

class TamanhosController extends Controller
{
    public function index() {
        $tamanhos = Tamanho::all();
        return view('tamanhos.index', compact('tamanhos'));
    }

    public function create() {
        return view('tamanhos.create');
    }

    public function store(TamanhoRequest $request) {
        $novo_tamanho = $request->all();
        Tamanho::create($novo_tamanho);
        return redirect()->route('tamanhos');
    }

    public function destroy($id) {
        Tamanho::find($id)->delete();
        return redirect()->route('tamanhos');
    }

    public function edit($id) {
        $tamanhos = Tamanho::find($id);
        return view('tamanhos.edit', compact('tamanhos'));
    }

    public function update(TamanhoRequest $request, $id) {
        Tamanho::find($id)->update($request->all());
        return redirect()->route('tamanhos');
    }
}