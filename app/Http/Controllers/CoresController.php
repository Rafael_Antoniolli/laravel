<?php

namespace App\Http\Controllers;

use App\Cor;
use Illuminate\Http\Request;
use App\Http\Requests\CorRequest;

class CoresController extends Controller
{
    public function index() {
        $cores = Cor::all();
        return view('cores.index', compact('cores'));
    }

    public function create() {
        return view('cores.create');
    }

    public function store(CorRequest $request) {
        $nova_cor = $request->all();
        Cor::create($nova_cor);
        return redirect()->route('cores');
    }

    public function destroy($id) {
        Cor::find($id)->delete();
        return redirect()->route('cores');
    }

    public function edit($id) {
        $cores = Cor::find($id);
        return view('cores.edit', compact('cores'));
    }

    public function update(CorRequest $request, $id) {
        Cor::find($id)->update($request->all());
        return redirect()->route('cores');
    }
}
