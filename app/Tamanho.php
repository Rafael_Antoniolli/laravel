<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tamanho extends Model
{
    protected $fillable = ['tamanho'];

    public function camisetas() {
        return $this->hasMany('App\Camiseta');
    }
}

