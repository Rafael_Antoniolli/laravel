<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camiseta extends Model
{
    protected $fillable = ['marca_id', 'tamanho_id', 'cor_id'];

    public function marca() {
        return $this->belongsTo('App\Marca');
    }

    public function tamanho() {
        return $this->belongsTo('App\Tamanho');
    }

    public function cor() {
        return $this->belongsTo('App\Cor');
    }
}
